Pod::Spec.new do |s|
  s.name         = "chcsv"
  s.version      = "0.0.2"
  s.summary      = "chcsv."
  s.description  = "chcsv  "
  s.homepage     = "http://www.pacer.cc"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "Ray" => "guolei@me.com" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "git@bitbucket.org:pacerhealth/chcsv.git", :tag => "0.0.2" }
  s.source_files  = "*.{h,m}"
  s.requires_arc = false
end
